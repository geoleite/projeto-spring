package br.com.web1.controller;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.web1.model.Aluno;

@RestController
@RequestMapping("servico1")
public class Servico1Controller {

	@GetMapping(value = "/getGeral")
	public Object getGeral() {
		// Conexao com o banco
		// Driver, Url, Usario e Senha
		List<Aluno> list = new ArrayList<Aluno>();
		try {
			String driver = "org.postgresql.Driver";
			String url = "jdbc:postgresql://localhost:5432/postgres";
			String usuario = "postgres";
			String senha = "postgres";
			// Como conectar
			Class.forName(driver);
			Connection con = DriverManager.getConnection(url, usuario, senha);
			PreparedStatement ps = con.prepareStatement("select * from aluno");
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				Aluno aluno = new Aluno();
				aluno.setCodigo(rs.getInt("id"));
				aluno.setEndereco("");
				aluno.setFone("");
				aluno.setMatricula(rs.getString("matricula"));
				aluno.setNome(rs.getString("nome"));
				list.add(aluno);
			}
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	@GetMapping(value = "/getById")
	public Aluno getById(@RequestBody Aluno aluno) {
		aluno = new Aluno();
		aluno.setCodigo(1);
		aluno.setMatricula("122134");
		aluno.setEndereco("Endereço");
		aluno.setFone("7928376498");
		aluno.setNome("Metodo get no corpo do request");
		return aluno;
	}

	@GetMapping(value = "/getById/{codigo}")
	public Aluno getById(@PathVariable int codigo) {
		Aluno aluno = new Aluno();
		aluno.setCodigo(1);
		aluno.setMatricula("122134");
		aluno.setEndereco("Endereço");
		aluno.setFone("7928376498");
		aluno.setNome("Método Get na URL");
		return aluno;
	}

	@PostMapping
	public Aluno post(@RequestBody Aluno aluno) {
		aluno.setNome("George");
		return aluno;
	}

	@PutMapping
	public Aluno put(@RequestBody Aluno aluno) {
		aluno.setNome("George");
		return aluno;
	}

	@DeleteMapping
	public Aluno delete(@RequestBody Aluno aluno) {
		return aluno;
	}
}
