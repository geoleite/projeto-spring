package br.com.web1.model;

import lombok.Data;

@Data
public class Aluno {

	private int codigo;
	private String matricula;
	private String nome, fone, endereco;
	
}
